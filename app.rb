require 'sinatra'
require 'sass'
require 'liquid'
require 'kramdown'

#Enable SASS
set :views, :sass => 'assets/css', :haml => 'templates', :default => 'views'

helpers do
	def find_template(views, name, engine, &block)
		_, folder = views.detect { |k,v| engine == Tilt[k] }
		folder ||= views[:default]
		super(folder, name, engine, &block)
	end
end

#Config Files
set :public_folder, File.dirname(__FILE__) + '/assets'
set :views, settings.root + '/templates'
set :bind, '192.168.1.2'
set :port, 8080
Liquid::Template.file_system = Liquid::LocalFileSystem.new(File.join(File.dirname(__FILE__),'templates/includes'))
Tilt.register :liquid, Tilt[:liquid]
Tilt.register :md, Tilt[:markdown]

#Pages
get '/' do
	liquid :index, :locals => { :title => 'OpenBook Platform' }
end

#Assets
get '/css/*.css' do
	file = params[:splat].first
	sass file.to_sym
end
